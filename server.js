const express = require('express');

const app = express();

app.set('port', (process.env.PORT || 3000));

app.get('/', (req, res) => {
  res.end('URL Shortener Microservice');
});

app.get('/new/:url(*)', (req, res) => {
  const regex = /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
  if (req.params.url.match(regex)) {
    const obj = {
      original: req.params.url,
      short: 'todo'
    }
    res.json(obj);
    res.end();
  } else {
    res.json({
      "error":"Wrong url format, make sure you have a valid protocol and real site."
    });
    res.end();
  }
});

app.listen(app.get('port'), () => {
  console.log('Server is up and listening on port', app.get('port'));
});
